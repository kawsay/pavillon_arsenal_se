require 'nokogiri'
require 'byebug'
require 'csv'

URL = 'https://www.pavillon-arsenal.com/fr/et-demain-on-fait-quoi/'
RSC_PATH  = File.join(__dir__, '..', 'resources')
HTML_PATH = File.join(RSC_PATH, 'html')
HTML_FILE = File.join(HTML_PATH, 'et-demain-on-fait-quoi.html')
CSV_PATH  = File.join(RSC_PATH, 'csv')
CSV_FILE  = File.join(CSV_PATH, 'categories_url_date.csv')
TXT_PATH  = File.join(RSC_PATH, 'txt')

def create_folders
  puts '[*] Creating directories...'
  [RSC_PATH, HTML_PATH, CSV_PATH, TXT_PATH].each do |path|
    Dir.mkdir(path) unless File.directory?(path)
  end
  puts '[+] Done'
end

def download_homepage
  puts '[*] Downloading ' + URL + '...'
  begin
  `wget -q -O #{HTML_FILE} #{URL}`
  rescue
    puts '[-] WGET is not installed on your machine'
    puts '[*] Check http://gnuwin32.sourceforge.net/packages/wget.htm'
    puts '[*] or    https://www.addictivetips.com/windows-tips/install-and-use-wget-in-windows-10/'
  end
  puts '[+] Done'
end

def list_categories
  puts '[*] Parsing categories...'

  html_file  = File.join(HTML_PATH, 'et-demain-on-fait-quoi.html')
  doc        = Nokogiri::HTML(File.open(html_file))
  categories = doc.search('div.grid-item')

  rows = 0
  CSV.open(CSV_FILE, 'wb') do |csv|
    csv << ['title', 'href', 'author', 'date']
    categories.each do |cat|
      begin
        href   = cat.search('a').first.attributes['href'].value
        title  = cat.search('a').first.attributes['title'].value
        author = cat.search('.sub').text.match(/^(\D*)/)[0]
        author = 'A26' if author == 'A'                       # Weak RegEx :^)
        date   = cat.search('.sub').text.match(/\d+\w*\s\w+\s\d+/)[0]
        csv << [title, href, author, date]
      rescue
        byebug
        puts '[-] Error, exiting'
        exit 2
      end
      rows += 1
    end
  end
  
  puts "[+] #{rows} rows written"
end

def download_pages
  puts '[*] Downloading categories\' HTML content...'

  files = 0
  CSV.foreach(CSV_FILE, { headers: :first_row}) do |row|
    filename  = row[0].downcase.gsub(/\s/, '_').gsub(/[^éèêôûîïöçàa-z\-\_]/, '') + '.html'
    html_file = File.join(HTML_PATH, filename)
    `wget -q -O #{html_file}.tmp #{row[1]}`
    File.open(html_file, 'w') do |file|
      file.puts row[1]
      File.foreach(html_file + '.tmp') do |line|
        file.puts line
      end
    end
    files += 1
  end
  puts "[+] #{files} pages downloaded"
end

def delete_html_tmp
  puts '[*] Deleting temporaries files...'
  Dir.glob(File.join(HTML_PATH, '*.tmp')).each do |file|
    File.delete(file)
  end
  puts '[+] Done'
end

def extract_articles
  puts '[*] Parsing and extracting pages\'s text...'
  html_pages = Dir["resources/html/*"].reject { |f| f.include?('et-demain-on-fait-quoi') }

  files = 0
  html_pages.each do |html_page|
    doc      = Nokogiri::HTML(File.open(html_page))
    filename = html_page.split('/').last + '.txt'
    txt_file = File.open(File.join(TXT_PATH, filename), 'w')

    url  = File.open(html_page, &:gets)
    text = doc.search('.article .txt').text
      .gsub(/\n/, ' ').gsub(/\r/, ' ').gsub(/\s{2,}/, ' ')
    
    txt_file.puts url
    txt_file.puts text
    files += 1
  end
  puts "[+] #{files} articles parsed"
end

def search_keywords
  library = {}
  files   = Dir[File.join(TXT_PATH, '*')]

  files.each do |file|
    lines = File.readlines(file)
    library[lines[0][0..-2]] = lines[1]
  end

  searching = true
  while searching
    puts '-> Keyword to lookup: '
    keyword = gets.chomp

    exit 0 if keyword == 'exit'

    r = library.select { |k, v| v.include?(keyword) }
    r.each { |result| puts result[0] }
  end
end

def run
  create_folders
  download_homepage
  list_categories
  download_pages
  delete_html_tmp
  extract_articles
  search_keywords
end

run
